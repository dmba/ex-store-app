package com.dmba.exstore.api.ofy;

import com.dmba.exstore.api.model.orm.DataEntity;
import com.google.appengine.api.datastore.Query;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static com.google.appengine.api.datastore.Query.*;

/**
 * Created by dmba on 10/10/15.
 */
public class DataService {

    static {
        ObjectifyService.register(DataEntity.class);
    }

    public DataEntity createUpdate(DataEntity entity) {
        ofy().save().entity(entity).now();
        return entity;
    }

    public DataEntity read(Long id) {
        return ofy().load().type(DataEntity.class).id(id).now();
    }

    public List<DataEntity> list() {
        List<DataEntity> list = ofy().load().type(DataEntity.class).list();
        Collections.sort(list, new Comparator<DataEntity>() {
            @Override
            public int compare(DataEntity o1, DataEntity o2) {
                if (o1 != null && o2 != null) {
                    if (o1.getModifiedDate() != null & o2.getModifiedDate() != null) {
                        return o2.getModifiedDate().compareTo(o1.getModifiedDate());
                    }
                }
                return 0;
            }
        });
        return list;
    }

    public List<DataEntity> list(String key) {
        return ofy().load().type(DataEntity.class).filter("key", key).list();
    }

    public void delete(DataEntity entity) {
        ofy().delete().entity(entity).now();
    }

    private Objectify ofy() {
        return ObjectifyService.ofy();
    }

}
package com.dmba.exstore.api.spi;

import com.dmba.exstore.api.model.rest.DataRequest;
import com.dmba.exstore.api.model.rest.Data;
import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Nullable;

import javax.inject.Named;
import java.util.List;

import static com.google.api.server.spi.config.ApiMethod.HttpMethod;

/**
 * Created by dmba on 10/10/15.
 */
@Api(
        name = "store",
        version = "v1",
        description = "SNB data storage API"
)
public class StoreEndpoint {

    private StoreProxy proxy = new StoreProxy();

    @ApiMethod(
            name = "data.get",
            path = "data",
            httpMethod = HttpMethod.GET
    )
    public List<Data> get(@Nullable @Named("key") String key) {
        return key == null ? proxy.getAll() : proxy.getByKey(key);
    }

    @ApiMethod(
            name = "data.getById",
            path = "data/{id}",
            httpMethod = HttpMethod.GET
    )
    public Data getById(@Named("id") Long id) {
        return proxy.getById(id);
    }

    @ApiMethod(
            name = "data.post",
            path = "data",
            httpMethod = HttpMethod.POST
    )
    public List<Data> post(DataRequest data) {
        return proxy.postNewData(data);
    }

    @ApiMethod(
            name = "data.update",
            path = "data/",
            httpMethod = HttpMethod.PUT
    )
    public List<Data> update(DataRequest request) {
        return proxy.update(request);
    }

    @ApiMethod(
            name = "data.delete",
            path = "data/{id}",
            httpMethod = HttpMethod.DELETE
    )
    public void delete(@Named("id") Long id) {
        proxy.delete(id);
    }

}


package com.dmba.exstore.api.spi;

import com.dmba.exstore.api.model.rest.DataRequest;
import com.dmba.exstore.api.ofy.DataService;
import com.dmba.exstore.api.model.Mapper;
import com.dmba.exstore.api.model.orm.DataEntity;
import com.dmba.exstore.api.model.rest.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmba on 10/10/15.
 */
public class StoreProxy {

    private DataService service = new DataService();
    private Mapper mapper = new Mapper();

    public void delete(Long id) {
        DataEntity entity = service.read(id);
        if (entity == null) {
            throw new IllegalArgumentException("No instance with id = " + id);
        } else {
            service.delete(entity);
        }
    }

    public Data getById(Long id) {
        DataEntity entity = service.read(id);
        if (entity == null) {
            throw new IllegalArgumentException("No instance with id = " + id);
        } else {
            return mapper.map(entity);
        }
    }

    public List<Data> getByKey(String key) {
        List<DataEntity> list = service.list(key);
        if (list == null) {
            throw new IllegalArgumentException("No instance with key = " + key);
        } else {
            return mapper.map(list);
        }
    }

    public List<Data> getAll() {
        List<DataEntity> list = service.list();
        if (list == null) {
            throw new IllegalStateException("No items found");
        } else {
            return mapper.map(list);
        }
    }

    public List<Data> postNewData(DataRequest request) {
        if (request == null || request.isEmpty()) {
            throw new IllegalArgumentException("Received empty data");
        }
        List<DataEntity> entityList = new ArrayList<DataEntity>();
        for (Data data : request.getData()) {
            DataEntity entity = service.createUpdate(new DataEntity(data.getId(), data.getKey(), data.getValue()));
            entityList.add(entity);
        }
        return mapper.map(entityList);
    }


    public List<Data> update(DataRequest request) {
        List<Data> list = new ArrayList<Data>();
        for (Data data : request.getData()) {
            Data d = update(data);
            list.add(d);
        }
        return list;
    }

    public Data update(Data data) {
        DataEntity entity = service.read(data.getId());

        if (entity == null) {
            throw new IllegalArgumentException("No instance with id = " + data.getId());
        } else {
            boolean modified = false;
            if (data.getKey() != null) {
                entity.setKey(data.getKey());
                modified = true;
            }
            if (data.getKey() != null) {
                entity.setValue(data.getValue());
                modified = true;
            }
            System.out.println(entity);
            if (modified) {
                service.createUpdate(entity);
            }
            return mapper.map(entity);
        }
    }

}

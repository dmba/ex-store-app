package com.dmba.exstore.api.model.rest;

import java.util.List;
import java.util.Map;

/**
 * Created by dmba on 10/14/15.
 */
public class DataRequest {

    private List<Data> data;

    public DataRequest() {
    }

    public List<Data> getData() {
        return data;
    }

    public boolean isEmpty() {
        return data == null || data.isEmpty();
    }

}
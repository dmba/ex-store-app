package com.dmba.exstore.api.model.orm;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;

import java.util.Date;

/**
 * Created by dmba on 10/10/15.
 */
@Entity
public class DataEntity {

    @Id
    Long id;
    @Index
    String key;
    Object value;
    Date modified;

    public DataEntity() {
    }

    public DataEntity(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public DataEntity(Long id, String key, Object value) {
        this.id = id;
        this.key = key;
        this.value = value;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }

    public Date getModifiedDate() {
        return modified;
    }

    @Override
    public String toString() {
        return "DataEntity{" +
                "id=" + id +
                ", key='" + key + '\'' +
                ", value=" + value +
                '}';
    }

    @OnSave
    void onSave() {
        modified = new Date();
    }

}

package com.dmba.exstore.api.model.rest;

import java.util.Date;

/**
 * Created by dmba on 10/10/15.
 */
public class Data {

    private Long id;
    private String key;
    private Object value;
    private Date modified;

    public Data() {
        this(null, null);
    }

    public Data(String key, Object value) {
        this(null, key, value, null);
    }

    public Data(Long id, String key, Object value, Date modified) {
        this.id = id;
        this.key = key;
        this.value = value;
        this.modified = modified;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Long getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }

    public Date getModified() {
        return modified;
    }

    @Override
    public String toString() {
        return "Data{" +
                "id=" + id +
                ", key='" + key + '\'' +
                ", value=" + value +
                ", modified=" + modified +
                '}';
    }

}

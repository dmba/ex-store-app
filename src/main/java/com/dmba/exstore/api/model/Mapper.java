package com.dmba.exstore.api.model;

import com.dmba.exstore.api.model.orm.DataEntity;
import com.dmba.exstore.api.model.rest.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dmba on 10/10/15.
 */
public class Mapper {

    public Data map(DataEntity entity) {
        return new Data(
                entity.getId(),
                entity.getKey(),
                entity.getValue(),
                entity.getModifiedDate()
        );
    }

    public DataEntity map(Data data) {
        return new DataEntity(data.getKey(), data.getValue());
    }

    public List<Data> map(List<DataEntity> entityList) {
        List<Data> dataList = new ArrayList<Data>();
        for (DataEntity e : entityList) {
            dataList.add(map(e));
        }
        return dataList;
    }

}